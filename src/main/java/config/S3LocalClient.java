package config;

import software.amazon.awssdk.services.s3.S3Client;

public class S3LocalClient {
    private static final S3Client S3_CLIENT = S3Client.builder().build();

    public static S3Client getS3Client() {
        return S3_CLIENT;
    }
}
