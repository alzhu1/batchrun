import batch.BatchApi;

/**
 * main executor
 */
public class Main {
    public static void main(String[] args) {
        String bucketName = "albert-test-3322-new";
        BatchApi.uploadFile(bucketName);
    }
}
