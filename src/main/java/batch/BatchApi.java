package batch;

import config.S3LocalClient;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

/**
 * Batch API for S3 operation
 */
public class BatchApi {
    private static final String PREFIX = "Albert/";

    public static void uploadFile(String bucketName) {
        String fileName = PREFIX.concat(String.valueOf(System.currentTimeMillis()));
        PutObjectRequest request = PutObjectRequest.builder()
            .bucket(bucketName)
            .key(fileName)
            .build();
        RequestBody requestBody = RequestBody.fromString("AlbertTest".concat(String.valueOf(System.currentTimeMillis())));
        S3LocalClient.getS3Client().putObject(request, requestBody);
    }
}